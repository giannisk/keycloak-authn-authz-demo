# Keycloak AuthN and AuthZ Demo
This is a simple Vue.js application that demonstrates the authentication and authorization capabilities of Keycloak.

![Preview](src/assets/preview.png)

Based on David Truxall's [vue-keycloak-demo](https://github.com/davetrux/vue-keycloak-demo).

## Quickstart Guide
1. Start Keycloak (e.g., `bin/kc.sh start-dev`).
2. Create a realm named *isslab*.
3. Create a user within the *isslab* realm.
4. Create a client named *isslab-client* within the *isslab* realm.
5. Clone this repository.
6. Run `npm install` to install the dependencies.
7. Run `npm run serve` to serve the application.
8. Test authentication and authorization with Keycloak. By default, the user cannot access the page which is subject to authorization.
9. Create a client role named *isslab-client-role* and assign it to the user. If you do so, the user will be authorized to access that page.

Optional: Edit `src/plugins/authentication.js` and/or `src/router/index.js` to make changes as needed.

For additional information regarding Keycloak, please consult the official [documentation](https://www.keycloak.org/documentation).

## License
Final work is made available under the the [MIT License](LICENSE).

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      isAuthenticated: false
    }
  },
  {
    path: '/authorized',
    name: 'Authorized',
    meta: {
      isAuthenticated: true
    },
    component: () => import('../views/Authorized.vue')
  },
  {
    path: '/unauthorized',
    name: 'Unauthorized',
    meta: {
      isAuthenticated: false
    },
    component: () => import('../views/Unauthorized.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  if (to.meta.isAuthenticated) {
    // Get the URL of the application which is needed for Keycloak.
    const basePath = window.location.toString()

    if (!Vue.$keycloak.authenticated) {
      // If the user is not authenticated, prompt the user to login.
      Vue.$keycloak.login({ redirectUri: basePath.slice(0, -1) + to.path })
    }

    else if (Vue.$keycloak.hasResourceRole('isslab-client-role')) {
      // If the user is authenticated and authorized, update the token.
      // TODO: Set the required role as needed.
      Vue.$keycloak.updateToken(70)
        .then(() => {
          next()
        })
        .catch(err => {
          console.error(err)
        })
    }

    else {
      // If the user is authenticated but not authorized, redirect the user.
      next({ name: 'Unauthorized' })
    }

  }

  else {
    // The page does not require authentication.
    next()
  }

})

export default router

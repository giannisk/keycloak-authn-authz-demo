import Vue from 'vue'
import Keycloak from 'keycloak-js'

// TODO: If Keycloak < 17.0.0, set http://localhost:8080/auth/ as the URL.
// TODO: If Keycloak >= 17.0.0, set http://localhost:8080/ as the URL.
// TODO: Set realm and clientId as needed.
const options = {
  url: 'http://localhost:8080/auth/',
  realm: 'isslab',
  clientId: 'isslab-client'
}

const _keycloak = Keycloak(options)

const Plugin = {
  install(Vue) {
    Vue.$keycloak = _keycloak
  }
}

Plugin.install = Vue => {
  Vue.$keycloak = _keycloak
  Object.defineProperties(Vue.prototype, {
    $keycloak: {
      get() {
        return _keycloak
      }
    }
  })
}

Vue.use(Plugin)

export default Plugin 
